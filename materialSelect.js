function templateFill(id, data) {
    let replaceText = document.getElementById(id).innerHTML;
    let keys = Object.keys(data);
    for (let i = 0; i < keys.length; i++) {
        let key = keys[i].replace('{', '').replace('}', '');
        let replace = new RegExp('{' + key + '}', "g");
        replaceText = replaceText.replace(replace, data[key]);
    }
    // Clean out any unsued variables (set to '')
    let replace = new RegExp('{\\S*}', "g");
    replaceText = replaceText.replace(replace, "");
    return replaceText;
}
function selectItem(item, className) {
    items = item.parentNode.getElementsByClassName(className);
    for (let i = 0; i < items.length; i++) { items[i].classList.remove(className + '-selected') };
    item.classList.add(className + '-selected');
}
function deselect(parentDiv, className) {
    items = parentDiv.getElementsByClassName(className);
    for (let i = 0; i < items.length; i++) { items[i].classList.remove(className + '-selected') };
}
function getSelectionItem(selectionId, className) {
    selectedElement = document.getElementById(selectionId).getElementsByClassName(className + '-selected')[0];
    return [selectedElement.parentNode.parentNode.id, selectedElement.dataset.value];
}
function optionDelete(option) {
    eval(option.parentNode.dataset.deleteFunction + '(\"' + option.dataset.value + '\");');
}
function filterItems(value, items) {
    for (let i = 0; i < items.length; i++) {
        if (items[i].dataset.value.toLowerCase().includes(value.toLowerCase())) { items[i].classList.remove('hidden'); }
        else { items[i].classList.add('hidden'); };
    }
}
function buildMaterialSelect(div, data) {
    let options = "";
    let selectFunction = '';
    if (data['selectFunction']) {
        selectFunction = data['selectFunction'];
    }
    for (let i = 0; i < data['items'].length; i++) {
        options += templateFill('template-material-scroll-item', {
            'name': data['items'][i],
            'id': data['items'][i],
            'order': i,
            'icon': data['icon'],
            'function': data['function'],
            'selectFunction':selectFunction,
        });
    }
    let searchAdd = '';
    let searchAddClass = '';
    if (data['searchAdd']) {
        searchAdd = templateFill('template-material-scroll-search-add', {function:data['searchAdd']})
        searchAddClass = 'material-ScrollSearchAdd';
    };
    // TODO: replace this object with a variable, then just push keys
    // to it as needed. Will clean things up a lot.
    let output = templateFill('template-material-scroll-list', {
        'title':data['title'],
        'options':options,
        'searchAdd':searchAdd,
        'searchAddClass':searchAddClass,
    });
    div.innerHTML = output;
}
